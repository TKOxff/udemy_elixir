defmodule Mix.SCM.Git do
  @behaviour Mix.SCM

  def init(arg) do
  end

  @impl Mix.SCM
  def fetchable? do
    true
  end

  @impl Mix.SCM
  def format(opts) do
    opts[:git]
  end
end
