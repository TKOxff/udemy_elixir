#
# The unquote Function
# page.309
#
defmodule My do
  defmacro macro(code) do
    quote do
      # quote 블럭 내부에서는, Elixir는 그냥 보통코드로 파싱한다. 그래서 code는 문자 그대로 code가 된다.
      IO.inspect(code)
    end
  end
end

defmodule Test do
  require My

  My.macro(IO.puts("hello in the puts"))
end

# code/0 미선언 함수라는 오류가 발생
# ** (CompileError) eg2.exs:17: undefined function code/0
