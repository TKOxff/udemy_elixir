#
# Using the Representation as Code
# page.308 macro/eg1.exs
#
defmodule My do
  defmacro macro(code) do
    IO.inspect(code)
    IO.puts("===")
    # We use quote to generate the internal form
    # 내부형식을 생성하기 위해 quote를 사용
    # quote로 지정된 코드조각을 리턴하고 실행한다.
    quote do: IO.puts("Different code")
  end
end

defmodule Test do
  require My

  # 매크로를 호출하는 지점에서 인자는 {튜플 내부표현}으로 전해진다.
  My.macro(IO.puts("hello in the puts"))
end

# 매크로에 인자로 넘어간 code는 튜풀내부표현으로 절대 실행되지는 않는다.
# {{:., [line: 22], [{:__aliases__, [line: 22], [:IO]}, :puts]}, [line: 22], ["hello"]}
# ===
# Different code
