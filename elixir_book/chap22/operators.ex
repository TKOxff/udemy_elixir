#
# Macros and Operators
#
defmodule Operators do
  # 매크로를 사용해 커널 연산자를 재정의 할 수 있다.
  # +를 문자열 붙이는 동작으로 변환
  defmacro a + b do
    quote do
      to_string(unquote(a)) <> to_string(unquote(b))
    end
  end
end

defmodule Test do
  IO.puts(123 + 456)
  import Kernel, except: [+: 2]
  # 매크로 정의는 지역적이며 is lexically scoped.
  # +연산자 재정의는 import Operators모듈을 임포트한 스코프 안에서만 유효하다!
  import Operators
  IO.puts(123 + 456)
end

IO.puts(123 + 456)

# output
#
# 579
# 123456
# 579
