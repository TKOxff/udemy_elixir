defmodule My do
  # 매크로는 인자를 평가(실행)되지 않는 *튜플표현으로 받는다.
  # (일반함수의 인자는 이미 평가(실행)되어 받아진다)
  defmacro myif(condition, clauses) do
    # 튜플표현의 do, else절을 추출하고 코드를 생성할 준비를 마친다.
    do_clause = Keyword.get(clauses, :do, nil)
    else_clause = Keyword.get(clauses, :else, nil)

    # 코드생성을 위해 quote블럭을 시작한다, unquote는 quote안에서만 사용 가능하므로.
    quote do
      # case문은 조건문을 평가하기 위해 조건문-코드조각을 unquote로 삽입한다.
      # *매크로 인자의 튜플표현을 실행가능한 코드조각으로 변환
      case unquote(condition) do
        # 조건문이 네거티브 값이라면 else절을 실행
        val when val in [false, nil] ->
          # else절을 실행하기 위해 unquote로 else절 코드조각을 삽입한다.
          # *내부표현을 실행가능한 코드조각으로 변환
          unquote(else_clause)

        _ ->
          unquote(do_clause)
      end
    end
  end
end

defmodule Test do
  require My

  My.myif 1 == 2 do
    IO.puts("1 == 2")
  else
    IO.puts("1 != 2")
  end
end
