#
# The unquote Function
# page.309
#
defmodule My do
  defmacro macro(code) do
    quote do
      # unquote로 잠시 quoting을 끄고 코드조각을 삽입한다.
      IO.inspect(unquote(code))
    end
  end
end

defmodule Test do
  require My

  My.macro(IO.puts("hello in the puts"))
end

# hello in the puts
# :ok
