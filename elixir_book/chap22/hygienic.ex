# Macros Are Hygienic 하이'지닉: 위생적인
defmodule Scope do
  # 매크로의 지역성을 확인: 매우 위정적인다~
  #
  # Macro definition has both its own scope and
  # a scope during execution of the quoted macro body.
  # 매크로의 선언은 각자의 소코프를 가진다.
  defmacro update_local(val) do
    local = "some value"

    result =
      quote do
        local = unquote(val)
        IO.puts("End of macro body, local = #{local}")
      end

    IO.puts("In macro definition, local = #{local}")
    result
  end
end

defmodule Test do
  require Scope

  # 결과를 보면 모든 스코프가 분리된 것을 볼 수 있다.
  local = 123
  Scope.update_local("cat")
  IO.puts("On return, local=#{local}")
end

## output
#
# In macro definition, local = some value
# End of macro body, local = cat
# On return, local=123
