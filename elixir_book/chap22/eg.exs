defmodule My do
  defmacro macro(code) do
    quote do
      # 평가되지 않는 quote 블럭 안에서 일부 코드는 평가되도록 한다
      IO.puts(unquote(code))
    end
  end
end

defmodule Test do
  require My
  My.macro(IO.puts("hello"))
end
