#
# Using the Representation as Code
# page.308 macro/eg0.exs
#
defmodule My do
  # a macro that simply returns its parameter (after printing it out).
  # 단순히 인자를 그대로 반환하는 매크로
  defmacro macro(code) do
    # 인자로 들어온 code는 내부표현 상태
    IO.inspect(code)
    IO.puts("===")
    # 반환 될 때 컴파일 트리 속으로 삽입되고 (평가되어 실행된다)
    code
  end
end

defmodule Test do
  require My

  # when we invoke the macro is passed as an internal representation.
  # 매크로를 호출하는 지점에서 인자는 {튜플 내부표현}으로 전해진다.
  My.macro(IO.puts("hello in the puts"))
end

#
# {{:., [line: 22], [{:__aliases__, [line: 22], [:IO]}, :puts]}, [line: 22], ["hello in the puts"]}
# ===
# hello in the puts
