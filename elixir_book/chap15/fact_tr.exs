

# tail recursive version
defmodule TailRecursive do
  def factorial(n), do: _fact(n, 1)
  defp _fact(0, acc), do: acc
  defp _fact(n, acc) do: _fact(n-1, acc*n)
end

# non-tail recursive
defmodule NonTailRecursive do
  def factorial(0), do: 1
  # 마지막 처리가 재귀호출이 아니고 n * 연산이다.
  def factorial(n), do: n * factorial(n-1)
end
