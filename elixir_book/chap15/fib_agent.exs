#
# Elixr Process has the STATE!
# Elxir modules cannoot hold state but Elixir Process does
#
# Agent module wrap a process containing state in a nice module interface.
#
defmodule FibAgent do
  def start_link do
    Agent.start_link(fn -> %{ 0 => 0, 1 => 1 } end)
  end

  def fib(pid, n) when n >= 0 do
    Agent.get_and_update(pid, &do_fib(&1, n))
  end

  defp do_fib(cache, n) do
    case cache[n] do
      nil ->
        { n_1, cache } = do_fib(cache, n - 1)
        result = n_1 + cache[n - 2]
        { result, Map.put(cache, n, result) }

        cache_value ->
          { cache_value, cache }
    end
  end
end

{:ok, agent} = FibAgent.start_link()
IO.puts FibAgent.fib(agent, 2000)
