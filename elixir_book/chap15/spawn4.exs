
defmodule Spawn4 do
  def greet do
    receive do
      {sender, msg} ->
        send sender, {:ok, "Hello, #{msg}"}

        # tail recursion = tail call optimization = no adding stack-freame
        # Elixir does NOT have loops, but RECURSION.
        #
        # 함수의 마지막 처리가 자신을 호출하는 경우라면, 꼬리호출? 최적화
        # 함수 호출이 발생하지 않고, 단지 jump back 함수의 시작라인으로 되돌아 가서 실행된다.
        # 한수인자가 있다면 대체 된다.
        # * 반드시 자신의 호출이 함수의 *마지막 처리이어야 한다.
        greet()
    end
  end
end

# here is a client
pid = spawn(Spawn4, :greet, [])

send pid, {self(), "World!"}
receive do
  {:ok, message} ->
    IO.puts message
end

send pid, {self(), "Kermit!"}
receive do
  {:ok, message} ->
    IO.puts message
  after 500 ->
    IO.puts "The greeter has gone away"
end
