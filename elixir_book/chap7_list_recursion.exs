#
# Chapter 7 Lists and Recursion
#

# List is made by Recursion.

defmodule MyList do

  # recursion을 이용하는 것의 list구현의 핵심
  def len([]), do: 0
  def len([ _head | tail ]), do: 1 + len(tail)

  # Create a new list
  def square([]), do: []
  def square([ head | tail ]), do: [head*head | square(tail)]

  def add_1([]), do: []
  def add_1([head | tail]), do: [ head+1 | add_1(tail) ]

  # *** map function 함수형 프로그래밍의 map함수 구현 원리!!! page.75
  #
  # Example
  #  MyList.map [1,2,3], fn x -> x * 2 end
  #  [2, 4, 6]
  #  MyList.map [1,2,3], &(&1 * 2)
  #  [2, 4, 6]
  #  MyList.map [1,2,3], &(&1 > 1)
  #  [false, true, true]
  def map([], _func), do: []
  def map([ head | tail ], func), do: [ func.(head) | map(tail, func) ]

  # *** reduce function 함수형 프로그래밍의 reduce함수 구현 원리!!!
  # value == initail value == accumulator
  #
  # Example
  #  MyList.reduce([1,2,3,4,5], 0, &(&1 + &2))  # &1=list &2=value=accumulator
  #  15
  #  MyList.reduce([1,2,3,4,5], 1, &(&1 * &2))
  #  120
  #
  def reduce([], value, _fun), do: value
  def reduce([head | tail], value, func), do: reduce(tail, func.(head, value), func)
  #
  # [head | tail]로 리스트를 받아서, 재귀호출에 tail list를 사용, 익명함수에 head 값과 value합산값으로 func를 호출!
  # tail이 앞으로 이동하므로 원소는 하나씩 감소하여 첫번째 anchor정의에 매칭되고 끝난다.

  # Exercise: ListAndResursion-1
  def mapsum([], _func), do: 0
  def mapsum([head | tail], func), do: func.(head) + mapsum(tail, func)

  # Exercise: ListAndResursion-2
  def max([m]), do: m
  def max([head | tail]), do: Kernel.max(head, max(tail))

  # def max([m]), do: m
  # def max([head | tail]), do: if head > max(tail), do: max(head), else: max(tail)

  # Exerise: ListAndRecursion-5

  # all? my-ver
  def all?([], value, _func), do: value
  def all?([ head | tail ], true, func), do: all?(tail, func.(head), func)
  def all?([ _head | _tail ], false, _func), do: false
  def all?([ head | tail ], func), do: all?(tail, func.(head), func)

  # all? book-ver
  def all_2?(list), do: all_2?(list, fn x -> !!x end)
  def all_2?([], _fun), do: true
  def all_2?([head | tail], fun) do
    if fun.(head) do
      all_2?(tail, fun)
    else
      false
    end
  end

  # each my-ver
  def each([], _fun), do: :ok
  def each([head | tail], fun) do
    fun.(head)
    each(tail, fun)
  end

  # each book-ver
  def each_2([], _fun), do: []
  def each_2([ head | tail ], fun) do
  [ fun.(head) | each_2(tail, fun) ]
  end

  # filter my-ver * better
  def filter([], _fun), do: []
  def filter([head | tail], fun) do
    if fun.(head) do
      [head | filter(tail, fun)]
    else
      filter(tail, fun)
    end
  end

  # filter book-ver
  def filter_2([], _fun), do: []
  def filter_2([ head | tail ], fun) do
    if fun.(head) do
      [ head, filter_2(tail, fun) ]
    else
      [ filter_2(tail, fun) ]
    end
  end

  # split my-ver
  # def split([head | tail], 0), do: {[], [head | tail]}
  # def split([head | tail], count) when count >= length([head | tail]), do: {[head | tail], []}

  # def split([head | tail], count) do
  #   if length(tail) == count do
  #     { [head], tail }
  #   else
  #     { [head], split(tail, count)}
  #   end
  # end

  # book-ver
  # def split(list, count), do: _split(list, [], count)
  # defp _split([], front, _), do: [Enum.reverse(front), []]
  # defp _split(tail, front, 0), do: [Enum.reverse(front), tail]
  # defp _split([head | tail], front, count) do
  #   _split(tail, [head | front], count-1)
  # end

  # entry func.
  def split(list, count) do
    IO.puts("split0")
    _split(list, [], count)
  end

  # anchor func. out of range case. [] back list is empty
  defp _split([], front, _) do
    IO.puts("split1")
    [Enum.reverse(front), []]
  end

  # anchor func. end func.
  defp _split(tail, front, 0) do
    IO.puts("split2")
    [Enum.reverse(front), tail]
  end

  # main recursion func. 원래 배열에서, head를 하나씩 빼서 새 리스트를 생성, 그리고 한번 뒤집고
  defp _split([head | tail], front, count) do
    IO.puts("split3 back:#{inspect([head|tail])} front:#{inspect(front)} count:#{count}")
    _split(tail, [head | front], count-1)
  end

  # take my-ver
  def take(list, count), do: _take(list, [], count)

  def _take([], front, _), do: Enum.reverse(front)
  def _take(_tail, front, 0), do: Enum.reverse(front)

  def _take([head | tail], front, count) do
    _take(tail, [head | front], count-1)
  end

  # take book-ver
  def take2(list, n), do: hd(split(list, n))

  # flatten my-ver
  def flatten(list) do
    _flatten(list, [])
  end

  def _flatten([], result) do
    result
  end

  #  [1 | []] = [1]
  # [[1 | []], 2] = [[1], 2]
  # [[1 | [3 | []]], 2] = [[1, 3], 2]
  #
  # 1, 2번에서 list로 만들어서 3번으로 보내는 것이 핵심?
  def _flatten([[h | []] | tail], result) do
    IO.puts("flatten1")
    _flatten([h | tail], result)
  end

  def _flatten([[h | t] | tail], result) do
    IO.puts("flatten2")
    _flatten([h, t | tail], result)
  end

  def _flatten([head | tail], result) do
    IO.puts("flatten3 head:#{inspect(head)} tail:#{inspect(tail)} flat:#{inspect(result)}")
    _flatten(tail, [head | result])
  end

  # flatten book-ver
  # def flatten(list), do: _flatten(list, [])

  # defp _flatten([], result), do: result

  # # head가 리스트 인데, 뒷가 비었다면 그건 버린다.
  # defp _flatten([[h | []] | tail], result) do
  #   IO.puts("flatten1 h:#{inspect(h)} t:[] tail:#{inspect(tail)}")
  #   _flatten([h | tail], result)
  # end

  # # head가 리스트이고, head-tail가있다면, [head-head, head-tail | tail] 새로운 리스트 생성
  # # 원소 1개의 리스트는 [head | []] = [1] 이다. [1 | []]
  # defp _flatten([[h | t] | tail], result) do
  #   IO.puts("flatten2 h:#{inspect(h)} t:#{inspect(t)} tail:#{inspect(tail)}")
  #   _flatten([h, t | tail], result)
  # end

  # 1,2번으로 가공해서 3번에 들오오게 한다..
  # defp _flatten([head | tail], result) do
  #   IO.puts("flatten3 tail:#{inspect(tail)}")
  #   _flatten(tail, [head | result])
  # end


end
