
# Chapter 5. Anonymous Functions

greet = fn -> IO.puts "Hello" end

greet.()

f1 = fn a, b -> a * b end

IO.puts f1.(5, 6)


f2 = fn -> 99 end

IO.puts f2.()


print_info = fn item ->
  IO.puts("#{inspect(IEx.Info.info(item))}")
end

# Funtions and Pattern Matching

swap = fn {a, b} -> {b, a} end

swap.( { 1, 9 } )
|> IO.inspect()

# Exercise: Functions-1
#Q1
list_concat = fn [a, b], [c, d] -> [a, b, c, d] end

ret = list_concat.([:a, :b], [:c, :d])
ret
|> IO.inspect()
|> print_info.()

#Q2
sum = fn a, b, c -> a + b + c end

IO.inspect sum.(1, 2, 3)

#Q3

pair_tuple_to_list = fn {a, b} -> [a, b] end

pair_tuple_to_list.({1234, 4567})
|> IO.inspect
|> print_info.()

#Exercise: Functions-2
foo = fn
  0, 0, _ -> "FizzBuzz"
  0, _, _ -> "Fizz"
  _, 0, _ -> "Buzz"
  _, _, c -> c
end

IO.inspect foo.(0, 0, 3)
IO.inspect foo.(0, 2, 3)
IO.inspect foo.(1, 0, 3)
IO.inspect foo.(1, 2, 3)

#Exercise: Functions-3

bar = fn n ->
  foo.(rem(n, 3), rem(n, 5), n)
end

IO.inspect bar.(10)
IO.inspect bar.(11)
IO.inspect bar.(12)
IO.inspect bar.(13)
IO.inspect bar.(14)
IO.inspect bar.(15)
IO.inspect bar.(16)


# Function Case Return Functions

fun1 =  fn ->
          fn ->
            "Hello"
          end
        end

IO.inspect fun1.().()

fun2 = fn -> (fn -> "Hello" end) end

IO.inspect inner = fun2.()
IO.inspect inner.()

# Function Remember Their Original Environment
# Closure - the scope encloses the bindings of its variables,
#  packaging them into something that can be SAVED and USED LATER.

greeter =
  fn name -> # name is parameter of outer func
    fn -> #! inner func uses other func's param
      "Hello #{name}"
    end
  end

# 내부 함수를 획득한다.
# outer func call returns inner func
dave_greeter = greeter.("Dave")

# 이미 실행이 끝난 외부 함수의 인자를 사용한다.. 그것이 클로져! 특성
# Something strange happens here
# inner func uses other func's param that outer func has already finished executing!!!
# "Dave"인자의 사용은 끝난 이 지점에서, 그 "Dave"인자를 사용하여 동작한다.
IO.inspect dave_greeter.()

# This works because
# functions in Elixir, automatically carry with them the bindings of variables
# in the scope in which they are defined.
# 이것이 동작하는 이유는
# Elixir에서 함수는, 자신이 선언된 scope의 묶인 변수들과 같이 자동으로 옮겨지기 때문이다.

# name인자는 외부 함수와 엮이고, 내부함수가 정의될때 외부함수 scope를 상속하여 name변수를 엮어 옮긴다.
# 이것이 크로져 Closure이다.
# the scope encloses the bindings of its variables,
# packaging them into something that can be saved and used later.
# 정의된 스코프의 변수들을 엮어 저장해 두고, 나중에 호출될 수 있도록 한다!! <- ***

# Parameterized Functions
add_n
  = fn n ->
      (fn other -> n + other end)
    end

add_two = add_n.(2) # add_two saved 2 in its bindings environment.
add_five = add_n.(5) # add_two saved 5

IO.inspect add_two.(8)
IO.inspect add_five.(5)

# Exercise: Functions-4
prefix
  = fn str1 ->
      fn str2 -> "#{str1} #{str2}" end
    end

mrs = prefix.("Mrs")
IO.puts mrs.("Smith")
IO.puts prefix.("Elixir").("Rocks")

IO.puts "\n## Passing Functions as Arguments ##\n"

times_2 = fn n -> n * 2 end
apply = fn (fun, value) -> fun.(value) end

IO.puts apply.(times_2, 6)

# Enum.map(a collection, a apply function) :: list
list = [1, 2, 3, 4]

map_return = Enum.map(list, fn elem -> elem * 2 end)
IO.inspect map_return
print_info.(map_return)
IO.inspect Enum.map(list, fn elem -> elem * elem end)
IO.inspect Enum.map(list, fn elem -> elem < 3 end)


IO.puts "\n## Pinned Values and Functions Parameters ##\n"
# ^ pin operator

defmodule Greeter do
  def for(name, greeting) do
    # returns inner func (nested func)
    fn
      (^name) -> "#{greeting} #{name}"
      (_) -> "I don't know you"
    end
  end
end

# inner func holds "Jose" as name and "Oi"
mr_valim = Greeter.for("Jose", "Oi!")

IO.puts mr_valim.("Jose") # => Oi! Jose
IO.puts mr_valim.("Dave") # => I don't know you

IO.puts "\n## The & Notation ##\n"
# creating short helper function is so commom

add_one = &(&1 + 1) # fn (n) -> n + 1 end
IO.puts add_one.(44)

square = &(&1 * &1) # fn (n) -> n * N end
IO.puts square.(8)

speak = &(IO.puts(&1)) # fn (n) -> IO.puts(n) end ## optimized ## &IO.puts/1
IO.puts speak.("Hello")

rnd = &(Float.round(&1, &2)) # opimized ok with correct order &Float.round/2
IO.puts rnd.(1.5, 0)
print_info.(rnd)

rnd2 = &(Float.round(&2, &1)) # opimized failuar with incorrect order
IO.puts rnd2.(0, 1.5)
print_info.(rnd2)

# & capture operator works with
s = &"bacon and #{&1}" # works with string
IO.puts s.("custard")

match_end = &~r/.*#{&1}$/ # works with string-like
IO.puts "cat" =~ match_end.("t")
IO.puts "cat" =~ match_end.("!")

# & function capture operator.
# can give it  the named existing functions.

l = &length/1 # &:erlang.length/1
IO.puts l.([1,2,3,4])

len = &Enum.count/1 # &Enum.count/1
IO.puts len.([1,2,3,4])

m = &Kernel.min/2 # &:erlang.min/2 This is an alias for Erlang function.
IO.puts m.(99,88)

# The & shortcut gives us a wonderful way to pass functions to other functions.
IO.inspect Enum.map [1,2,3,4], &(&1 + 1)
IO.inspect Enum.map [1,2,3,4], &(&1 * &1)
IO.inspect Enum.map [1,2,3,4], &(&1 < 3)
# VS
IO.inspect Enum.map list, fn elem -> elem * 2 end
IO.inspect Enum.map list, fn elem -> elem * elem end
IO.inspect Enum.map list, fn elem -> elem < 3 end

# Exercise: Functions-5
IO.inspect Enum.map [1,2,3,4], fn x -> x + 2 end
IO.inspect Enum.map [1,2,3,4], &(&1 + 2)

IO.inspect Enum.map [1,2,3,4], fn x -> IO.inspect x end
IO.inspect Enum.map [1,2,3,4], &(IO.inspect(&1))
