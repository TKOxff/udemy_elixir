use Mix.Config

config :issues, github_rul: "https://api.github.com"

config :logger, compile_time_purge_level: :info
