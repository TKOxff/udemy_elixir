defmodule Issues.MixProject do
  use Mix.Project

  def project do
    [
      app: :issues,
      escript: escript_config(),
      version: "0.1.0",
      elixir: "~> 1.11",
      name: "Issues",
      # source_url: "https://github/TKoxff/issues",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  # 별개로 돌아가는 프로세스 애플리케션
  def application do
    [
      extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.0.0"},
      {:poison, "~> 1.0.0"},
      {:ex_doc, "~> 0.19"},
      # {:earmark, "~> 1.2.4"},
    ]
  end

  defp escript_config do
  [
    main_module: Issues.CLI
  ]
  end
end
