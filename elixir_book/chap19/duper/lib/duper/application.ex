defmodule Duper.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: Duper.Worker.start_link(arg)
        Duper.Results,
      { Duper.PathFinder, "/Users/tkoxff/proj" }, # . is current working directory
        Duper.WorkerSupervisor,
      { Duper.Gatherer, 1 },
    ]

    opts = [strategy: :one_for_all, name: Duper.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
