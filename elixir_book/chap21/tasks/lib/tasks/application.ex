defmodule Tasks.Application do
  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # {Task, fn -> do_something_extraordinary() end}
      {Tasks.MyTask, 123}
    ]

    Supervisor.start_link(children, strategy: :one_for_one)
  end
end
