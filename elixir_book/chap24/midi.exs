defmodule Midi do
  defstruct(content: <<>>)

  defmodule Frame do
    defstruct(type: "xxxx", length: 0, data: <<>>)

    def to_binary(%Midi.Frame{type: type, length: length, data: data}) do
      <<
        type::binary-4,
        length::integer-32,
        data::binary
      >>
    end
  end

  def from_file(name) do
    %Midi{content: File.read!(name)}
  end
end

defimpl Enumerable, for: Midi do
  def _reduce(_content = "", {:cont, acc}, _fun) do
    {:done, acc}
  end

  def _reduce(
        <<
          type::binary-4,
          length::integer-32,
          data::binary-size(length),
          rest::binary
        >>,
        {:cont, acc},
        fun
      ) do
    frame = %Midi.Frame{type: type, length: length, data: data}
    _reduce(rest, fun.(frame, acc), fun)
  end

  def reduce(%Midi{content: content}, state, fun) do
    _reduce(content, state, fun)
  end

  def count(midi = %Midi{}) do
    frame_count = Enum.reduce(midi, 0, fn _, count -> count + 1 end)
    {:ok, frame_count}
  end

  def member?(%Midi{}, %Midi.Frame{}) do
    # 에러를 발생시키는 것이 아니고, 기본 구현을 호출한다.
    {:error, __MODULE__}
  end

  def slice(%Midi{}) do
    # 에러를 발생시키는 것이 아니고, 기본 구현을 호출한다.
    {:error, __MODULE__}
  end
end
