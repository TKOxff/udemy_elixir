defmodule Sequence.MixProject do
  use Mix.Project

  def project do
    [
      app: :sequence,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    IO.puts("mix.exs application call")

    [
      extra_applications: [:logger],
      # mod option, tells OTP the module that is the main entry point.
      mod: {
        # top-level module
        Sequence.Application,
        []
      },
      env: [
        initial_number: 456
      ],
      # second option
      registered: [
        Sequence.Server
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:distillery, "~> 1.5", runtime: false}
    ]
  end
end
