defmodule Sequence.Impl do
  #
  # Business logic
  #
  def next(number), do: number + 1

  def increment(number, delta), do: number + delta
end
