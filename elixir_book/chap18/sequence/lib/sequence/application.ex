#
# Application Specification
# 어플리케이션 명세 파일
#
defmodule Sequence.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    initial_number = Application.get_env(:sequence, :initial_number)
    IO.puts("Sequence.Application start initial_number:#{initial_number}")

    children = [
      # Starts a worker by calling: Sequence.Worker.start_link(arg)
      {Sequence.Stash, initial_number},
      {Sequence.Server, nil}
    ]

    IO.puts("Application start")

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :rest_for_one, name: Sequence.Supervisor]

    # args = list of child, options
    # *** Supervisor의 핵심 역활은 관리하는 프로세스가 죽었을때 재실행 하는 것
    # *** process has no memory of its past lives,
    # *** and no state is retained across a crash.
    Supervisor.start_link(children, opts)
  end
end
