#
# Woker process
#
defmodule Sequence.Server do
  use GenServer, restart: :transient

  @vsn "0"

  # Forwards calls on the server implementions (Impl)
  alias Sequence.Impl

  @server Sequence.Server

  ###
  # External API
  def start_link(_) do
    IO.puts("Server start_link")
    GenServer.start_link(@server, nil, name: @server)
  end

  def next_number do
    GenServer.call(@server, :next_number)
  end

  def increment_number(delta) do
    GenServer.cast(@server, {:increment_number, delta})
  end

  # GenServer implementation
  def init(_) do
    IO.puts("Server init")

    {:ok, Sequence.Stash.get()}
  end

  def handle_call(:next_number, _from, current_number) do
    {:reply, current_number, Impl.next(current_number)}
  end

  def handle_cast({:increment_number, delta}, current_number) do
    {:noreply, Impl.increment(current_number, delta)}
  end

  # 종료될때(크래쉬?) 현재 state을 Stash에 저장한다
  def terminate(_reason, current_number) do
    IO.puts("Server terminate")
    Sequence.Stash.update(current_number)
  end

  def format_status(_reason, [_pdict, state]) do
    [data: [{'State', "My current state is '#{inspect(state)}', and I'm happy"}]]
  end
end
