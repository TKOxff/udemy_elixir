#
# Chapter 6 Modules and Named Functions
#

defmodule Times do
  def double(n) do
    n * 2
  end

  # The functions's Body is a Block!
  # fun(), do: .. 가 컴파일된 후의 형식
  def square(n), do: n * n
  
  # Exercise: ModulesAndFunctions-1
  # ..
end

