defmodule Discuss.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  schema "users" do
    field :name, :string
    field :username, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username])
    |> validate_required([:name, :username])
    |> unique_constraint(:username)
  end

  def search(query, search_term) do
    wildcard_search = "%#{search_term}%"

    IO.puts ">>>begin"
    IO.inspect query
    IO.inspect search_term
    IO.puts ">>>end"

    from user in query,
    where: like(user.name, ^wildcard_search),
    or_where: like(user.username, ^wildcard_search)
  end

end
