
defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards
  """

  @spec create_deck :: [<<_::24, _::_*8>>, ...]
  @doc """
    this is functions doc
  """
  def create_deck do
    # 12. Comprehensions Over Lists
    values = ["Ace", "Two", "Three", "Four", "Five"]
    suits = ["Spades", "Clubs", "Hearts", "Diamons"]

    # a list comprehensions
    # solution.1
    # cards = for value <- values do
    #   for suit <- suits do
    #     "#{value} of #{suit}"
    #   end
    # end
    # List.flatten(cards)

    # solution.2 is much better.
    for suit <- suits, value <- values do
      "#{value} of #{suit}"
    end
  end

  # 8. Method Arguments
  # func/arity - The number of arguments
  #
  # 10. Immutability in Elixir
  # Elixir never ever are modifying an existing data structure in place.
  #
  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  # 11. Searching a List
  #
  @doc """
    Determines wheter a deck contains a given card

    ## Examples

      iex> deck = Cards.create_deck
      ["Ace of Spades", "Two of Spades", "Three of Spades", "Four of Spades",
      "Five of Spades", "Ace of Clubs", "Two of Clubs", "Three of Clubs",
      "Four of Clubs", "Five of Clubs", "Ace of Hearts", "Two of Hearts",
      "Three of Hearts", "Four of Hearts", "Five of Hearts", "Ace of Diamons",
      "Two of Diamons", "Three of Diamons", "Four of Diamons", "Five of Diamons"]
      iex> Cards.contains?(deck, "Ace of Spades")
      true

  """
  def contains?(deck, card) do
    Enum.member?(deck, card)
  end

  @doc """
    Divides a deck into a hand and the remainder of the deck
    The `hand_size` argument indicates how many cards should
    be in the hand.

  ## Examples

      iex> deck = Cards.create_deck
      iex> {hand, _deck} = Cards.deal(deck, 1)
      iex> hand
      ["Ace of Spades"]

  """
  def deal(deck, hand_size) do
    Enum.split(deck, hand_size) # return tuple { }
  end

  # 18. Saving a Deck
  # call :erlang system function
  def save(deck, filename) do
    binary = :erlang.term_to_binary(deck)
    File.write(filename, binary)
  end

  # def load(filename) do
  #   {status, binary} = File.read(filename)
  #   :erlang.binary_to_term(binary)
  # end

  @spec load(
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | char,
              binary | []
            )
        ) :: any
  def load(filename) do
    # 한번에 두가지 분리된 처리를 수행한다.
    # 결과 성공여부를 비교하고, 실제 값을 획득한다.
    case File.read(filename) do
      {:ok, binary} -> :erlang.binary_to_term(binary)
      {:error, _reason} -> "That file does not exist"
    end
  end

  @spec create_hand(integer) :: {[any], [any]}
  def create_hand(hand_size) do
    # deck = Cards.create_deck
    # deck = Cards.shuffle(deck)
    # hand = Cards.deal(deck, hand_size)

    # |> 파이프 연산자를 사용하면 아래의 형식의 된다.
    # 함수의 리턴값은 다음 함수의 첫번째 인자값으로 삽입 된다.
    Cards.create_deck
    |> Cards.shuffle
    |> Cards.deal(hand_size) # hand_size는 두번째 인자
  end

end
