defmodule Concat do

  def join(a, b \\ nil, sep \\ "+")

  def join(a, b, _sep) when is_nil(b) do
    IO.puts "join with is_nil guard."
    a
  end

  def join(a, b, sep) do
    IO.puts "no default params"
    a <> sep <> b
  end

end

defmodule DefaultTest do

  def dowork(x \\ "hello") do
    x
  end
end
