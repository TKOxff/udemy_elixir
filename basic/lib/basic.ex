defmodule Basic do
  @moduledoc """
  Documentation for `Basic`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Basic.hello()
      :world

  """
  def hello do
    :world
  end

  def basic_op do

    # Elixir also provides ++ and -- to manipulate lists:
    l1 = [1, 2, 3] ++ [4, 5, 6]
    l2 = [1, 2, 3] -- [1, 3]

    IO.inspect(l1)
    IO.inspect(l2)

    # String concatenation <>
    str = "foo" <> "bar"
    IO.inspect(str)

    :ok
  end

  # = The match operator
  # ^ Pin Operator
  def pattern_matching do

    {a, b, c} = {:hello, "world", 43}

    IO.inspect(a)
    IO.inspect(b)
    IO.inspect(c)
  end

  def test_case(var) do

    case var do
      {4, 5, 6} ->
        "This clause won't match"
      {1, x, 3} ->
        IO.puts("This clause will match and bind x to 2 in the clause")
        IO.puts("matched x: #{x}")
      {2, y, 4} when y > 10 ->
        IO.puts("matched y: #{y}")
      _ ->
        "This clause would match any value"
    end
    |> IO.puts
  end

  def utf8_encoding do
    str = "hełło"
    IO.puts("String.length: #{String.length(str)}")
    IO.puts("byte_size: #{byte_size(str)}")
    
  end

end
