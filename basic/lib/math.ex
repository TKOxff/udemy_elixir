defmodule Math do
  def sum(a, b) do
    a + b
  end

  defp _do_sum(a, b) do
    a + b
  end

  # Function declarations also support guards and multiple clauses.
  def zero?(0) do
    true
  end

  def zero?(x) when is_integer(x) do
    false
  end


  def sum_list([head | tail], acc) do
    sum_list(tail, head + acc)
  end

  def sum_list([], acc) do
    acc
  end

  def double_each([head | tail]) do
    [head * 2 | double_each(tail)]
  end

  def double_each([]) do
    []
  end
end
