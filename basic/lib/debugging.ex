

(1..10)
|> IO.inspect
|> Enum.map(fn x -> x * 2 end)
|> IO.inspect
|> Enum.sum()
|> IO.inspect

[1, 2, 3]
|> IO.inspect(label: "before")
|> Enum.map(&(&1 * 2))
|> IO.inspect(label: "after")
|> Enum.sum()

defmodule Rnd do
  def some_fun(a, b, c) do
    IO.inspect binding()

    a + b + c
  end

  # def some_fun2(e, f, g) do
  #   require IEx; IEx.pry

  #   e + f + g
  # end
end

Rnd.some_fun(10, 200, 3000)

Rnd.some_fun2(1, 2, 3)
